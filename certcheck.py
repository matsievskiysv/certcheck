#!/usr/bin/env python3

from datetime import datetime
from dateutil import parser
from pathlib import Path
from sys import exit
import asyncio
import configparser
import enum
import smtplib
import argparse


class CertType(enum.Enum):
    X509 = enum.auto()
    CRL = enum.auto()


class Result(enum.Enum):
    NORMAL = enum.auto()
    WARNING = enum.auto()
    EXPIRED = enum.auto()


async def run(cmd):
    proc = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE)

    stdout, stderr = await proc.communicate()

    return proc.returncode, stdout, stderr


async def gather(*input_coroutines):
    res = await asyncio.gather(*input_coroutines, return_exceptions=False)
    return res


def commands_crl_date(paths):
    return [f"openssl crl -in {path} -noout -nextupdate" for path in paths]


def commands_x509_date(paths):
    return [f"openssl x509 -in {path} -noout -enddate" for path in paths]


def get_delta(date):
    cert_date = parser.parse(date)
    now = datetime.now(cert_date.tzinfo)
    delta = cert_date - now
    return delta.days


def produce_verdict(days, path, warn_days):
    if days == 0:
        return Result.EXPIRED
    elif days == warn_days:
        return Result.WARNING
    else:
        return Result.NORMAL


def send_message(message, sender, recievers, server, password, crl_expired):
    message = f"""From: {sender}
To: {recievers}
Subject: VPN certificate expiration

***This is an auto-generated e-mail message***

{message}
"""

    if crl_expired:
        message += """\nNote: connection is not possible to the \
VPN server with expired CRL certificate.\n"""

    smtpObj = smtplib.SMTP(server, 587)
    smtpObj.starttls()
    smtpObj.login(sender, password)
    smtpObj.sendmail(sender, recievers.split(","), message)
    smtpObj.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="checkcert.py",
        description="Check for certificates about to expire.")
    parser.add_argument("config_file", help="Configuration file.")
    args = parser.parse_args()

    config_file = Path(args.config_file)

    if not config_file.is_file():
        exit(f"'{args.config_file}' is not a file")

    config = configparser.ConfigParser()
    try:
        config.read(config_file)
    except configparser.MissingSectionHeaderError:
        exit(f"cannot parse configuration file")

    x509_files = [list(Path("/").glob("./" + g))
                  for g in config["certificates"]["x509"].split(",")]
    crl_files = [list(Path("/").glob("./" + g))
                 for g in config["certificates"]["crl"].split(",")]

    x509_list = list(zip(*x509_files, commands_x509_date(*x509_files),
                         [CertType.X509 for _ in range(0, len(*x509_files))]))
    crl_list = list(zip(*crl_files, commands_crl_date(*crl_files),
                        [CertType.CRL for _ in range(0, len(*crl_files))]))

    openssl_result = list(zip([x for x in asyncio.get_event_loop()
                               .run_until_complete(gather(
                                   *[run(c) for c in
                                     commands_x509_date(*x509_files) +
                                     commands_crl_date(*crl_files)]))],
                              x509_list + crl_list))

    delta = zip(openssl_result,
                list(map(lambda cert: get_delta(
                    cert[0][1].decode().split("=")[1].strip()),
                         openssl_result)))

    message = ""
    crl_expired = False
    for line in delta:
        for time in config["time"]["warning"].split(","):
            verdict = produce_verdict(line[1], line[0][1][0], int(time))
            if verdict != Result.NORMAL:
                crl_expired = crl_expired or (line[0][1][2] == CertType.CRL)
                if verdict == Result.EXPIRED:
                    message += f"Certificate {line[0][1][0].name} has expired.\n"
                    break
                else:
                    message += f"""Certificate {line[0][1][0].name} will \
expire in {time} days.\n"""

    if message != "":
        send_message(message,
                     config["email"]["address"],
                     config["email"]["recievers"],
                     config["email"]["server"],
                     config["email"]["password"],
                     crl_expired)
